use std::fs::File;
use std::io::Read;
use std::collections::HashSet;
use std::string::String;

fn main() {
    let mut file = File::open("resources/input.txt").expect("File not found");
    let mut input = String::new();
    let mut total = 0;
    let mut frequencies : HashSet<i32> = HashSet::new();
    let mut done = false;

    file.read_to_string(&mut input).expect("Could not read the file");

    while !done {
        for line in input.split("\n") {
            if !line.is_empty() {
                let num : i32 = line.parse().expect("Failed to parse line to int");
                
                total += num;

                if frequencies.contains(&total) {
                    println!("{}", total);
                    done = true;
                    break;
                } else {
                    frequencies.insert(total);
                }
            }
        }
    }
}
