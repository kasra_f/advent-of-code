extern crate regex;
use regex::Regex;
use std::fs::File;
use std::io::Read;
use std::string::String;
use std::collections::HashMap;
use std::cmp::Ordering;

#[derive(Debug)]
struct DateTime {
    year:  u16,
    month: u8,
    day:   u8,

    hour:   u8,
    minute: u8
}

impl std::cmp::PartialEq for DateTime {
    fn eq(&self, other: &DateTime) -> bool {
        return self.year == other.year &&
            self.month == other.month &&
            self.day == other.day &&
            self.hour == other.hour &&
            self.minute == other.minute;
    }
}

impl std::cmp::PartialOrd for DateTime {
    fn partial_cmp(&self, other: &DateTime) -> Option<Ordering> {
        if self.year > other.year {
            return Some(Ordering::Greater); }
        else if self.year < other.year {
            return Some(Ordering::Less); }
        else if self.month > other.month {
            return Some(Ordering::Greater); }
        else if self.month < other.month {
            return Some(Ordering::Less); }
        else if self.day > other.day {
            return Some(Ordering::Greater); }
        else if self.day < other.day {
            return Some(Ordering::Less); }
        else if self.hour > other.hour {
            return Some(Ordering::Greater); }
        else if self.hour < other.hour {
            return Some(Ordering::Less); }
        else if self.minute > other.minute {
            return Some(Ordering::Greater); }
        else if self.minute < other.minute {
            return Some(Ordering::Less); }
        else {
            return Some(Ordering::Equal); }
    }
}
    

#[derive(Debug)]
struct Log {
    time:    DateTime,
    message: String
}

impl std::cmp::Ord for Log {
    fn cmp(&self, other: &Log) -> Ordering {
        return self.time.partial_cmp(&other.time).expect("");
    }
}

impl std::cmp::PartialOrd for Log {
    fn partial_cmp(&self, other: &Log) -> Option<Ordering> {
        return self.time.partial_cmp(&other.time);
    }
}

impl std::cmp::PartialEq for Log {
    fn eq(&self, other: &Log) -> bool {
        return self.time.eq(&other.time);
    }
}

impl std::cmp::Eq for Log {
}


#[derive(Debug)]
struct Guard {
    id:             u16,
    minutes_asleep: HashMap<u8,u32>,
    total_asleep:   u32
}

impl std::cmp::Ord for Guard {
    fn cmp(&self, other: &Guard) -> Ordering {
        if self.total_asleep > other.total_asleep {
            return (Ordering::Greater);
        } else if self.total_asleep < other.total_asleep {
            return (Ordering::Less);
        } else {
            return (Ordering::Equal);
        }
    }
}


impl std::cmp::PartialOrd for Guard {
    fn partial_cmp(&self, other: &Guard) -> Option<Ordering> {
        if self.total_asleep > other.total_asleep {
            return Some(Ordering::Greater);
        } else if self.total_asleep < other.total_asleep {
            return Some(Ordering::Less);
        } else {
            return Some(Ordering::Equal);
        }
    }
}

impl std::cmp::PartialEq for Guard {
    fn eq(&self, other: &Guard) -> bool {
        return self.total_asleep == other.total_asleep;
    }
}

impl std::cmp::Eq for Guard {
}

    
fn main() {
    let mut file     = File::open("resources/input.txt").expect("File not found");
    let mut input    = String::new();
    let line_matcher = Regex::new(r"\[(?P<year>\d{4})\-(?P<month>\d{2})\-(?P<day>\d{2}) (?P<hour>\d{2}):(?P<minute>\d{2})\] (?P<message>.+)\n").unwrap();
    let mut logs     = Vec::<Log>::new();
    
    file.read_to_string(&mut input).expect("Could not read the file");

    for cap in line_matcher.captures_iter(input.as_str()) {    
        logs.push(Log {
            message: cap["message"].to_string(),
            time:    DateTime {
                year:   cap["year"].  parse::<u16>().expect("Failed to parse year"),
                month:  cap["month"]. parse::<u8>(). expect("Failed to parse month"),
                day:    cap["day"].   parse::<u8>(). expect("Failed to parse day"),
                hour:   cap["hour"].  parse::<u8>(). expect("Failed to parse hour"),
                minute: cap["minute"].parse::<u8>(). expect("Failed to parse minute"),
            }
        });
    }

    logs.sort();

    let guard_shift_matcher = Regex::new(r"Guard #(?P<id>\d+) begins shift").unwrap();
    let mut map = HashMap::<u16, Guard>::new();
    let mut asleep : u8 = 0;
    let mut id : u16 = 0;
    
    for log in &logs {
        let msg = &log.message;
        if guard_shift_matcher.is_match(msg) {
            let cap = guard_shift_matcher.captures(msg).unwrap();                   
            id  = cap["id"].parse::<u16>().expect("Failed to extract guard id");
            
            if !map.contains_key(&id) {
                let guard = Guard {id: id, minutes_asleep: HashMap::<u8,u32>::new(), total_asleep: 0};
                map.insert(id, guard);
            }
        } else {
            match map.get_mut(&id) {
                Some(guard) => {
                    if msg == "wakes up" {
                        for i in asleep..log.time.minute {
                            if guard.minutes_asleep.contains_key(&i) {
                                guard.minutes_asleep.insert(i, guard.minutes_asleep.get(&i).expect("") + 1);
                            } else {
                                guard.minutes_asleep.insert(i, 1);
                            }
                        }

                        guard.total_asleep += (log.time.minute as u32) - (asleep as u32);
                    } else if msg == "falls asleep" {
                        asleep = log.time.minute;
                    } else {
                        println!("Message not recognized: {}", msg);
                    }
                },
                None => {
                    println!("Invalid wake up. No user specified: {}", msg);
                }
            }
        }
    }

    let mut guard_list : Vec<&Guard> = Vec::<&Guard>::new();
    
    for entry in map.iter() {
        guard_list.push(entry.1);
    }

    guard_list.sort();

    for guard in guard_list {
        let mut count_vec: Vec<(&u8, &u32)> = guard.minutes_asleep.iter().collect();
        count_vec.sort_by(|a, b| b.1.cmp(a.1));
        println!("{}:: {:?}", guard.id, count_vec);
    }
}
