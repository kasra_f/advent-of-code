extern crate regex;
use regex::Regex;
use std::fs::File;
use std::io::Read;
use std::string::String;

struct Coord {
    x: usize,
    y: usize
}

struct Claim {
    id:      usize,
    padding: Coord,
    size:    Coord
}

struct Point {
    ids: Vec<usize>
}

fn main() {
    let mut file     = File::open("resources/input.txt").expect("File not found");
    let mut input    = String::new();
    let line_matcher = Regex::new(r"#(?P<id>\d+) @ (?P<px>\d+),(?P<py>\d+): (?P<sx>\d+)x(?P<sy>\d+)\n").unwrap();
    let mut claims   : Vec<Claim> = Vec::new();
    
    file.read_to_string(&mut input).expect("Could not read the file");

    for cap in line_matcher.captures_iter(input.as_str()) {    
        claims.push(Claim {
            id: cap["id"].parse::<usize>().expect("Failed to parse ID"),
            padding: Coord {
                x: cap["px"].parse::<usize>().expect("Failed to parse px"),
                y: cap["py"].parse::<usize>().expect("Failed to parse py")
            },
            size: Coord {
                x: cap["sx"].parse::<usize>().expect("Failed to parse sx"),
                y: cap["sy"].parse::<usize>().expect("Failed to parse sy")
            }
        });
    }

    // Get the size of the fabric
    let mut fabric : Vec<Vec<Point>> = Vec::with_capacity(1000);

    for _ in 0..1000 {
        let mut row = Vec::<Point>::with_capacity(1000);
        for _ in 0..1000 {
            row.push(Point{ids: Vec::new()});
        }
        fabric.push(row);
    }

    for claim in &claims {
        let x = claim.padding.x;
        let y = claim.padding.y;

        for i in x..x+claim.size.x {
            for j in y..y+claim.size.y {
                match fabric[i][j].ids.len() {
                    0 => {
                        fabric[i][j].ids.push(claim.id);
                    },
                    1 => {
                        fabric[i][j].ids.push(claim.id);
                    },
                    _ => {
                        fabric[i][j].ids.push(claim.id);
                    }
                }
            }
        }
    }

    for claim in &claims {
        let x = claim.padding.x;
        let y = claim.padding.y;
        let mut overlaps = false;

        for i in x..x+claim.size.x {
            for j in y..y+claim.size.y {
                match fabric[i][j].ids.len() {
                    0 => {
                        // Do nothing
                    },
                    1 => {
                        // Do nothing
                    },
                    _ => {
                        overlaps = true;
                        break;
                    }
                }
            }
        }

        if !overlaps {
            println!("ID: {}", claim.id);
        }
    }
}
